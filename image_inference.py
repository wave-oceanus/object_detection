# Example: python image_inference.py --modeldir=models/OceanusNet_100k/ --threshold=0.6 --image=images/IMAGENAME.png

import object_detection as OD
import argparse
import cv2
import os

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--modeldir', help='Folder the .tflite file is located in', required=True)
parser.add_argument(
    '--threshold', help='Minimum confidence threshold for displaying detected objects', default=0.5)
parser.add_argument(
    '--image', help='Name of the single image to perform detection on.', required=True)


args = parser.parse_args()
MODEL_NAME = args.modeldir
MODEL_NAME = os.path.join(os.path.dirname(__file__),MODEL_NAME)
IM_NAME = args.image
THRESHOLD = float(args.threshold)

objectDetector = OD.ObjectDetection(MODEL_NAME, THRESHOLD, False)
processedImage, detectionJSON = objectDetector.imageInference(cv2.imread(IM_NAME))
objectDetector.saveImage(IM_NAME, processedImage)